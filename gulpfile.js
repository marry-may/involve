let gulp = require('gulp'),
	browserSync = require('browser-sync'),
	concat = require('gulp-concat'),
	sass = require('gulp-sass'),
	imagemin = require('gulp-imagemin'),
	uglify = require('gulp-uglifyjs'),
	htmlmin = require('gulp-htmlmin'),
	del = require('del'),
	path = require('path');

const rollup = require('gulp-better-rollup');
const babel = require('rollup-plugin-babel');
const resolve = require('rollup-plugin-node-resolve');
const commonjs = require('rollup-plugin-commonjs');

/**
 * CSS Tasks
 */
function fonts(){
	return gulp.src('app/scss/fonts/*')
		.pipe(gulp.dest('dist/css/fonts'));
}
function cssimages(done){
	gulp.src([
		'app/scss/images/*',
	]).pipe(gulp.dest('dist/css/images'));
	done();
}
gulp.task('cssimages', cssimages);

const css = gulp.series(fonts, cssimages, function () {
	return  gulp.src([
		// '',
		// 'app/css/fonts.css',
		'app/scss/styles.scss'
	])
		.pipe(concat('style.scss'))
		.pipe(sass({
			includePaths: ['node_modules', 'bower_components']
		}))
		.pipe(gulp.dest('dist/css'));
});

/**
 * JavaScript TASK
 */
function js() {
	// Pages scripts
	return gulp.src([
		'app/js/app.js',
	])
		.pipe(rollup({ plugins: [babel(), resolve(), commonjs()] }, 'umd'))
		.pipe(gulp.dest('dist/js'));
}

function html() {
	return gulp.src('app/*.html')
		.pipe(htmlmin({collapseWhitespace: true}))
		.pipe(gulp.dest('dist'));
};

function images(done) {
	gulp.src('app/images/**/*')
	// .pipe(imagemin())
		.pipe(gulp.dest('dist/images'));
	done();
}

function clean(done) {
	return del.sync('dist', done());
}
function fa_icons() {
	return gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
		.pipe(gulp.dest('dist/css/fonts'));
}


const build = gulp.series(clean, gulp.parallel(css, js, images, html, fa_icons));

function watchFiles() {
	browserSync({
		server: {
			baseDir: 'dist',
			host: "192.168.5.214"
		},
		notify: false
	});
	var htmlWatcher = gulp.watch('app/**/*.html');
	htmlWatcher.on('change', function(path, stats) {
		html();
		browserSync.reload();
	});
	var jsWatcher = gulp.watch('app/js/**/*.js');
	jsWatcher.on('change', function(path, stats) {
		js();
		browserSync.reload();
	});
	var cssWatcher = gulp.watch('app/scss/**/*.scss');
	cssWatcher.on('change', function(path, stats) {
		css();
		browserSync.reload();
	});
	var imagesWatcher = gulp.watch('app/images/**/*');
	imagesWatcher.on('change', function(path, stats) {
		images();
		browserSync.reload();
	})
}

// Local Production Publisher
gulp.task('publish', gulp.series(css,js,function () {
	gulp.src([
		'dist/js/*'
	]).pipe(gulp.dest('../www/wp-content/themes/realestate/js/'));
	return gulp.src([
		'dist/css/*',
		'dist/css/**/*'
	]).pipe(gulp.dest('../www/wp-content/themes/realestate/css/'));
}));

// Add common tasks
gulp.task('fa-icons', fa_icons);
gulp.task('images', images);
gulp.task('css', css);
gulp.task('js', js);
gulp.task('html', html);

exports.default = gulp.series(build, watchFiles);